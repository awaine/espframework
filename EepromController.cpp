/*
 * Author Alex Waine & Richard Gaskin
 * 
 * Esp8266 Setup Framework 
 * 
 *  Version 1.0.0
 * 
*/


#include <ESP8266WiFi.h>
#include <EEPROM.h>
#include "EepromController.h" 

/*
 * Start EEPROM
 */
void EepromController::startEeprom(){
     EEPROM.begin(512);
     Serial.println();
     Serial.println("eeprom begin!!");
}


/*
 * Sotes Wifi Credentials
 */
void EepromController::storeWifiCreds(String ssid,String pass){

  
// Declare char arrays
  char ssidArray[50];
  char passArray[50];

  // Convert to char array
  ssid.toCharArray(ssidArray, ssid.length()+1);
  pass.toCharArray(passArray, pass.length()+1);


  //DELETE THIS ON PRODUCTION
  Serial.println(ssidArray);
  Serial.println(passArray);


  EEPROM.put(0, ssidArray);
  EEPROM.put(50, passArray);
  EEPROM.commit();


}

/*
 * Clears EEPROM
 */
void EepromController::wipe(){
  
   for (int i = 0 ; i < EEPROM.length() ; i++) {
        EEPROM.write(i, -1);
      }

       EEPROM.commit();
}


/*
 * Gets WiFi ssid from EEPROM
 */
 String EepromController::eepromGetWifiSsid(){

 // Declare char arrays
  char ssidArray[50];

  EEPROM.get(0, ssidArray);

    Serial.println(ssidArray);
    
  return ssidArray;
 }

/*
 * Gets WiFi pass from EEPROM
 */
 String EepromController::eepromGetWifiPass(){

 // Declare char arrays
  char passArray[50];

  EEPROM.get(50, passArray);

      Serial.println(passArray);

  return passArray;
 }
