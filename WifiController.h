/*
 * Author Alex Waine & Richard Gaskin
 * 
 * Esp8266 Setup Framework 
 * 
 *  Version 1.0.0
 * 
*/

 
#ifndef WifiController_h
#define WifiController_h

#include <ESP8266WiFi.h>

class WifiController{
public:
   String getSsidOptions();
   void setupWifiAp();
   void forgetWifi();
   void wifiConnTimer(int,String, String);
};

#endif
