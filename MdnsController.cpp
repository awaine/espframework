/*
 * Author Alex Waine & Richard Gaskin
 * 
 * Esp8266 Setup Framework 
 * 
 *  Version 1.0.0
 * 
*/

#include "MdnsController.h" 
#include <ESP8266mDNS.h>

void MdnsController::loopHandle(){
    MDNS.update();
}

void MdnsController::initMdns(){
  
   if (!MDNS.begin("esp8266")) {
    Serial.println("Error setting up MDNS responder!");
  }

  // Add service to MDNS-SD
  MDNS.addService("http", "tcp", 80);
  
  Serial.println("mDNS responder started");
}
