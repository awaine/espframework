/*
 * Author Alex Waine & Richard Gaskin
 * 
 * Esp8266 Setup Framework 
 * 
 *  Version 1.0.0
 * 
*/

#ifndef MdnsController_h
#define MdnsController_h

class MdnsController{
public:
   void loopHandle();
   void initMdns();
};

#endif
