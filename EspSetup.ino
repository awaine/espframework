
/*
 * Author Alex Waine & Richard Gaskin
 * 
 * Esp8266 Setup Framework 
 * 
 *  Version 1.0.0
 * 
*/

// Include external libs
#include <ESP8266WiFi.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>

// Include internal libs
#include "EepromController.h"
#include "MdnsController.h"
#include "WifiController.h"

// Import pages 
#import "Pages/WifiFormPage.h"
#import "Pages/ClearEepromPage.h"
#import "Pages/RestartPage.h"



// Pages declaration
const String wifiFormPage1 = WIFIFORM_page_1;
const String wifiFormPage2 = WIFIFORM_page_2;
const String RestartPage = RESTART_page;
const String ClearEepromPage = CLEAREEPROM_page;

// Internal variables
const byte DNS_PORT = 53;
String wifiOptions;
IPAddress dns_IP(192, 168, 1, 1);

// External Class instances 
DNSServer dnsServer;
ESP8266WebServer server(80);

// Internal Class instances 
EepromController EepromController;
//MdnsController MdnsController;
WifiController WifiController;

/*
 * Shows "Wifi SSID & pass form page"
 */
void noConRoot(){

    String page = wifiFormPage1 + wifiOptions + wifiFormPage2;

   server.send(200, "text/html", page);
}

/*
 * Sets WiFi Credentials in EEPROM
 */
void setCreds(){
  
  // Get input credentials
  String ssidInput= server.arg("ssid");
  String passInput= server.arg("pass");

  EepromController.wipe();
  EepromController.storeWifiCreds(ssidInput,passInput);

  server.send(200, "text/html", RestartPage);

  delay(10000);

  ESP.restart();
}

/*
 * Routes for not connected to WiFi state
 */
void notConnectedRoutes(){
   // Server routes
  server.on("/", noConRoot);
  server.on("/wificreds", setCreds);
  server.on("/generate_204", noConRoot); 
  server.on("/fwlink", noConRoot);
}



/*
 * Setup WiFi main function
 */
void setupCredsRoutine(){

   wifiOptions = WifiController.getSsidOptions();
   WifiController.setupWifiAp();
   dnsServer.start(DNS_PORT, "*", dns_IP);
  
      notConnectedRoutes();

   // Reply to all requests with same HTML
  server.onNotFound([]() {
    String page = wifiFormPage1 + wifiOptions + wifiFormPage2;
    server.send(200, "text/html", page );
  });
}

/*
 * Root for connected state
 */
void connectedRoot(){
      // TODO : make nice landing page for this
      server.send(200, "text/html", "connected  :)");
}

/*
 * Clear EEPROM page
 */
void clearEeprom(){

     EepromController.wipe();
     
     server.send(200, "text/html", ClearEepromPage);

      WifiController.forgetWifi();
 
      delay(10000);
 
   ESP.restart();
}

/*
 * Sets the routes for connected state
 */
void connectedRoutes(){
  server.on("/", connectedRoot);
  server.on("/eeprom/clear", clearEeprom);
}

/*
 * Main Setup
 */
void setup() {
   Serial.begin(115200);
   EepromController.startEeprom();
   
  // String ssid = EepromController.eepromGetWifiSsid();
   //String pass = EepromController.eepromGetWifiPass();
 //  WifiController.wifiConnTimer(20,ssid,pass);
  
  
  // DELETE THIS ON PRODUCTION 
  Serial.println();
  Serial.println("DELETE THIS ON PRODUCTION");
//  Serial.println(ssid);
 // Serial.println(pass);
  Serial.println();


    // WiFi Connected check 
  // if(WiFi.status() != WL_CONNECTED){
  //  setupCredsRoutine();
  //  Serial.println("please connect to WiFi");    
  // }else{
    
  //  Serial.println("Connected  :) ");
  //  connectedRoutes();
 //   MdnsController.initMdns();
//   }
//  server.begin();
}


/*
 * Main Loop
 */
void loop() {
     // Handles any incomming request
    dnsServer.processNextRequest();
    server.handleClient();
    if(WiFi.status() != WL_CONNECTED){
      
    }else{
//     MdnsController.loopHandle();
    }
    delay(1000);

}
