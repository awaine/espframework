/*
 * Author Alex Waine & Richard Gaskin
 * 
 * Esp8266 Setup Framework 
 * 
 *  Version 1.0.0
 * 
*/

#ifndef EepromController_h
#define EepromController_h

class EepromController{
public:
   void startEeprom();
   void wipe();
   void storeWifiCreds(String,String);
   String eepromGetWifiSsid();
   String eepromGetWifiPass();
};

#endif
